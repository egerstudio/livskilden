<?php namespace EgerStudio\EventCalendar;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    	return [
            'EgerStudio\EventCalendar\Components\EventList' => 'eventList',
            'EgerStudio\EventCalendar\Components\EventDetails' => 'eventDetails',
            'EgerStudio\EventCalendar\Components\Upcoming' => 'upcoming',
        ];
    }

    public function registerSettings()
    {
    }


}

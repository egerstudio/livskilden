<?php namespace EgerStudio\EventCalendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEgerstudioEventcalendarEvent2 extends Migration
{
    public function up()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->boolean('active');
        });
    }
    
    public function down()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->dropColumn('active');
        });
    }
}

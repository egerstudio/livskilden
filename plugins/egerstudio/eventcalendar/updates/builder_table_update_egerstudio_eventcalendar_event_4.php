<?php namespace EgerStudio\EventCalendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEgerstudioEventcalendarEvent4 extends Migration
{
    public function up()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->text('long_description');
            $table->string('ticket_link');
        });
    }
    
    public function down()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->dropColumn('long_description');
            $table->dropColumn('ticket_link');
        });
    }
}

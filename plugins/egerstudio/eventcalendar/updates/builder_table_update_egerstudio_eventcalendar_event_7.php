<?php namespace EgerStudio\EventCalendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEgerstudioEventcalendarEvent7 extends Migration
{
    public function up()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->text('og_description');
        });
    }
    
    public function down()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->dropColumn('og_description');
        });
    }
}

<?php namespace EgerStudio\EventCalendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEgerstudioEventcalendarEvent6 extends Migration
{
    public function up()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->text('details_subinfo');
        });
    }
    
    public function down()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->dropColumn('details_subinfo');
        });
    }
}

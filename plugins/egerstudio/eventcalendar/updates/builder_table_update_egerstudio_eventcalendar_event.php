<?php namespace EgerStudio\EventCalendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEgerstudioEventcalendarEvent extends Migration
{
    public function up()
    {
        Schema::rename('egerstudio_eventcalendar_', 'egerstudio_eventcalendar_event');
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::rename('egerstudio_eventcalendar_event', 'egerstudio_eventcalendar_');
        Schema::table('egerstudio_eventcalendar_', function($table)
        {
            $table->increments('id')->unsigned()->change();
        });
    }
}

<?php namespace EgerStudio\EventCalendar\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateEgerstudioEventcalendarEvent5 extends Migration
{
    public function up()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('egerstudio_eventcalendar_event', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}

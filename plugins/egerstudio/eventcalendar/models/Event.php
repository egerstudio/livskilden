<?php namespace EgerStudio\EventCalendar\Models;

use Model;

/**
 * Model
 */
class Event extends Model
{

    use \October\Rain\Database\Traits\Validation;

    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_eventcalendar_event';


    /**
     * QS for active events
     */
    public function scopeActive($query) 
    {

        return $query->where('active','=',1);
        
    }

    public function scopeFuture($query) 
    {

        return $query->where('date','>=',now());
        
    }


}

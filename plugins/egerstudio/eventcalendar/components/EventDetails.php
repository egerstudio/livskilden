<?php namespace Egerstudio\Eventcalendar\Components;

use Cms\Classes\ComponentBase;
use EgerStudio\EventCalendar\Models\Event;

class EventDetails extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'eventDetails Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['event'] = Event::where('slug','=',$this->param('slug'))->active()->first();
    }
}

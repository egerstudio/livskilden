<?php namespace Egerstudio\Eventcalendar\Components;

use Cms\Classes\ComponentBase;
use EgerStudio\EventCalendar\Models\Event;

class Upcoming extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'upcoming Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
      $events = new Event;
      $this -> page['upcomingevents'] = $events->active()->future()->orderBy('date','asc')->limit('6')->get();
    }

}

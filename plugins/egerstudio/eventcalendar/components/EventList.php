<?php namespace Egerstudio\Eventcalendar\Components;

use Cms\Classes\ComponentBase;
use EgerStudio\Eventcalendar\models\Event;

class EventList extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'eventList Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
      $events = new Event;
      $this -> page['events'] = $events->active()->future()->orderBy('date','asc')->get();
      
    }
}

<?php return [
    'plugin' => [
        'name' => 'Event Calendar',
        'description' => 'A calendar for showing events at Eger & Co.',
        'main-menu-title' => 'Arrangementer',
    ],
    'event' => [
        'tab' => [
            'details' => 'Detaljer',
            'images' => 'Bilder',
            'text' => 'Detaljert beskrivelse',
        ],
        'title' => 'Tittel',
        'description' => 'Beskrivelse',
        'date' => 'Dato & tid',
        'active' => 'Aktiv',
        'menu' => [
            'events' => 'Arrangementer',
        ],
        'image' => 'Annet bilde',
        'featured-image' => 'Hovedbilde',
        'long_text' => [
            'text' => 'Detaljert beskrivelse',
        ],
        'ticket_link' => 'Billettlink',
        'slug' => 'Slug',
        'details-subinfo' => 'Ekstra info',
        'og_description' => 'OpenGraph beskrivelse',
    ],
    'manage-events' => 'Behandle arrangementer',
    'manage-events-tab' => 'Arrangementer',
];